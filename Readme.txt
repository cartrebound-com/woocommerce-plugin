=== Cart Catch for WooCommerce - cart abandonment  ===
Contributors: rrrhys
Tags: cart abandonment, abandoned carts, send email, email customers, recover sales, follow up
Requires at least: 4.8
Tested up to: 5.0.0
WC requires at least: 2.6
WC tested up to: 3.5
Requires PHP: 5.6
Stable tag: trunk
License: GPLV2
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Allows WooCommerce store owners to recover lost sales, by emailing their customers.

== Description ==
Cart Catch allows WooCommerce stores to recover lost sales due to cart abandonment.

We use customisable, branded, personalised email campaigns to draw customers back to your store who failed to check out.

Completely free.

== Installation ==
1. Create an account at [https://app.cartcatch.com/](https://app.cartcatch.com) to receive your store ID and secret key.
2. Install and activate the plugin. Enter your store ID and secret key in the setup screen.
3. Open a new window on your store, add some items to your cart, visit the checkout page, add your email address, and close the window. We'll send you an email to remind you to checkout.
4. That's it! Start recovering sales.